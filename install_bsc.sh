#!/usr/bin/env bash

MPICH_BIN_PATH='/usr/bin/mpirun'
CONDA_BIN_PATH='/opt/conda/bin/conda'
CONDA_ENV='conda_py364_fges'

function install_conda() {
    if [ -f $CONDA_BIN_PATH ];
    then
        echo "Conda is already installed in your system"
    else
        echo "Conda not found in the system."
        echo "Installing conda..."

        cd / && \
        wget https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
        /bin/bash ~/miniconda.sh -b -p /opt/conda && \
        rm ~/miniconda.sh && \
        /opt/conda/bin/conda clean -tipsy && \
        ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
        echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
        export PATH=$CONDA_BIN_PATH:$PATH

        echo "Conda installation finished!"
    fi

    echo ""
}

function update_conda_env() {
    echo "Updating conda environment..."

    $CONDA_BIN_PATH env update -f ./conda_environment.yml
    if [ $? -eq 0 ]; then
        echo "Conda environment updated!"
        echo ""
    else
        echo "Conda environment is not created, please run the following and the run this script again: "
        echo "source ~/.bashrc"
        echo "conda env create python=3.6.4 -f ./conda_environment.yml"
       exit 1
    fi

    #Can't run the following because conda command is not found:
    #$CONDA_BIN_PATH env create python=3.6.4 -f ./conda_environment.yml
    #$CONDA_BIN_PATH activate $CONDA_ENV
}

function running_instructions() {
    echo "--------Installation DONE!----------"

    #Conda activate <env_name>:
    echo "Please now activate the conda environment by doing: "
    echo "echo 'conda activate $CONDA_ENV' >> ~/.bashrc"
    echo "source ~/.bashrc"

    echo "conda activate $CONDA_ENV"
    echo ""

    #As multiple nodes with MPI in a HCP environment:
    echo "------To run the program in multiple nodes (in a HPC cluster computing environment) through MPI:---------"
    echo "- Run the ./jobfile_bsc.sh"
    echo "- If there are any errors, please check the configuration content of the ./jobfile_bsc.sh file"
    echo "- NOTE: The default number of nodes to run this softare is set to 400 (standard PRACE queue in the BSC).
    If your queue is different please change the line 4 of the ./jobfile_bsc.sh file to change the --ntasks and --nodes
    to the number of nodes supported by your queue (for the BSC: 200 for RES Class A and B, 32 for RES Class B, 50 for
     BSC queue)."
}

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "ERROR: This script must be run as root" 1>&2
   echo "If you really think you don't need to run this as root, please edit this file and remove lines 69 to 74" 1>&2
   exit 1
fi

echo "Installing FGES software..."
install_conda
update_conda_env
running_instructions