#!/bin/bash

DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

NUM_TASKS=10
for ((i=1;i<=NUM_TASKS;i++)); do
    mpirun -np 3 --hostfile hosts_mpi.conf python FGES_parallel.py
    echo "Done MPI task: $i ; $DATE_NOW" >> log_mpi_tasks.log
done
