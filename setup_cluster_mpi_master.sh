#!/usr/bin/env bash

#Tutorial from: http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/
IP_SLAVES=('192.168.1.115' '138.100.11.135')

##### Create user for MPI tasks #########
#As root:
#userdel mpiuser
adduser --disabled-password --gecos "" $MPI_USERNAME
echo $MPI_USERNAME:cig_mpi1 | chpasswd #Set user password
usermod -aG sudo $MPI_USERNAME #Add user to sudo group

######## Setup ssh ##########
apt-get install openssh-server
#Allow firewall to accept ssh connections:
ufw allow ssh

#As mpiuser
su - mpiuser
ssh-keygen -t rsa

#Copy-paste the public key to the authorized_keys file in slaves machines (to access without prompting for password)
for i in "${IP_SLAVES[@]}":
do
    ssh-copy-id -i ~/.ssh/id_rsa.pub mpiuser@$i
done

######## Set up NFS ##########
SHARED_FOLDER='/home/mpiuser/workspace/'
mkdir -p $SHARED_FOLDER
cd $SHARED_FOLDER
git clone https://gitlab.com/mmichiels/fges_parallel.git
echo "Please now create a data folder in this directory and fill it with the desired datasets"

sudo apt-get install nfs-kernel-server
echo "$SHARED_FOLDER *(rw,sync,no_root_squash,no_subtree_check)" | sudo tee -a /etc/exports
sudo exportfs -a
sudo service nfs-kernel-server restart

echo "Setup master MPI finished!"
