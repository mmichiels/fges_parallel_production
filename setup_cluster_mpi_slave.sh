#!/usr/bin/env bash

#Tutorial from: http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/

IP_MASTER='138.100.13.35'
MPI_USERNAME='mpiuser'

##### Create user for MPI tasks #########
#As root:
#userdel mpiuser
adduser --disabled-password --gecos "" $MPI_USERNAME
echo $MPI_USERNAME:cig_mpi1 | chpasswd #Set user password
usermod -aG sudo $MPI_USERNAME #Add user to sudo group

######## Setup ssh ##########
apt-get install openssh-server
#Allow firewall to accept ssh connections:
ufw allow ssh

#As mpiuser
su - mpiuser
ssh-keygen -t rsa

#Copy-paste the public key to the authorized_keys file in master machine
ssh-copy-id -i ~/.ssh/id_rsa.pub mpiuser@$IP_MASTER

######## Setup NFS mounted folder #############
SHARED_FOLDER='/home/mpiuser/workspace/'
mkdir -p $SHARED_FOLDER

sudo apt-get -y install nfs-common
sudo mount -t nfs $IP_MASTER:$SHARED_FOLDER $SHARED_FOLDER
echo "$IP_MASTER:$SHARED_FOLDER $SHARED_FOLDER nfs" | sudo tee -a /etc/fstab

echo "Setup slave MPI finished!"
