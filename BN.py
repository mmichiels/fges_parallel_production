from sklearn.linear_model import LinearRegression
from sklearn.metrics import mutual_info_score
import scipy.stats as scipy_stats
from line_profiler import LineProfiler
from profilehooks import timecall
from profilehooks import profile
# from memory_profiler import profile as mem_profile
from pympler import tracker
from pympler import asizeof as aof
import numba
#from numba.pycc import CC
from decorator import decorator
from itertools import repeat
import multiprocessing as mp
#import torch
import os
import mkl
import time
import networkx as nx
import networkx.readwrite as networkx_io
import numpy as np
from itertools import combinations, permutations
import time
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from collections import namedtuple
import bisect
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from functools import wraps
import warnings
from os import environ as env
from numpy.linalg import inv
import math
from operator import itemgetter
from mpi4py import MPI
from io import StringIO
import sys
import json
import FGES_parallel
import BnParameters
import BnInference
#import BnApproxInferencePyro
import score_utils

class BN():
    def __init__(self, dataframe, graph=None, parameters=None):
        self.dataframe = dataframe
        self.graph = graph
        self.parameters = parameters

    def read_structure_from_file(self, graph_filepath):
        print("========= Reading graph structure from CSV file ============")

        filename, file_extension = os.path.splitext(graph_filepath)
        if file_extension == ".csv":
            graph_pd = pd.read_csv(graph_filepath, na_filter=False, dtype=np.float64, low_memory=False)
        elif file_extension == ".gzip":
            graph_pd = pd.read_parquet(graph_filepath, engine="fastparquet").astype(np.float64)
        else:
            raise Exception("File extension {} is not supported".format(file_extension))

        adj_matrix = graph_pd.iloc[:, 1:].values
        self.graph = adj_matrix

        return 0

    def learn_structure(self, algorithm_name, mode="local-global"):
        if algorithm_name == "fges":
            algorithm_parameters = {"backend": "neurosuites", "fges_penalty": 10.0}  # 4.34 for net1
            algorithm = FGES_parallel.FGES(self.dataframe, algorithm_parameters, data_type="continuous")

            print("======== Learning BN structure with: {} {}========".format(algorithm_name, mode))
            sys.stdout.flush()
            self.graph = algorithm.run(backend="neurosuites", mode=mode)
        else:
            raise Exception("Algorithm {} is not supported".format(algorithm_name))

        return 0

    def learn_parameters(self, algorithm_name):
        print("====== Learning parameters with {} ======".format(algorithm_name))

        if algorithm_name == "mle":
            gaussian_mle = BnParameters.GaussianMLE(self.dataframe, self.graph)
            self.parameters = gaussian_mle.run()
            print(gaussian_mle)
        else:
            raise Exception("Algorithm {} is not supported".format(algorithm_name))

        return 0

    def export_parameters(self):
        parameters_filepath = "parameters_exported.json"
        parameters_dict = {}
        for node, params in self.parameters.items():
            parameters_dict[node] = {
                "mean": params.mean,
                "variance": params.var,
                "parents_names": params.parents_names,
                "parents_coeffs": params.parents_coeffs,
            }

        with open(parameters_filepath, 'w') as file:
            json.dump(parameters_dict, file)

    def run_inference(self, algorithm_name, evidences, marginals):
        print("======= Running {} inference. Evidences: {} ; Marginals: {} ======".format(algorithm_name,
                                                                                          evidences, marginals))
        cols_names = self.dataframe.columns.values

        if algorithm_name == "exact":
            mu_joint, sigma_joint, order = BnInference.joint(self)
            mu_cond, sigma_cond, order_nodes = BnInference.condition_on_evidence(mu_joint, sigma_joint, cols_names, evidences)
            mu_marginal, sigma_marginal = BnInference.marginal(mu_cond, sigma_cond, order_nodes, marginals)

        elif algorithm_name == "NUTS" or algorithm_name == "variational":
            print("WARNING: This inference mode is not fully implemented. It's only computing the joint probability")

            model_pyro = BnApproxInferencePyro.ModelPyro(self, cuda=False)
            mu_marginal, sigma_marginal = model_pyro.run_inference(algorithm_name, show_plots=False)
            #bn_updated_parameters = model_pymc3.get_parameters_bn()
        else:
            raise Exception("Algorithm {} is not supported".format(algorithm_name))

        print("Mean marginals ({}):".format(marginals), mu_marginal)
        print("Sigma marginals ({}):".format(marginals), sigma_marginal)

        return mu_marginal, sigma_marginal


def run_multiple_nets(df, save_csv=True):
    dir = './tests_inference'
    nets_filenames = next(os.walk(dir))[2]

    dists_computed = {}
    cols_names = df.columns.values

    for net_name in nets_filenames:
        bn = BN(df)

        net_path = os.path.join(dir, net_name)
        bn.read_structure_from_csv(net_path)

        bn.learn_parameters("mle")

        evidences = {}
        marginals = cols_names
        mu_marginal, sigma_marginal = bn.run_inference("exact", evidences, marginals)
        dists_computed[net_name] = (mu_marginal, sigma_marginal)

    if save_csv:
        dir_save = os.path.join(dir, 'results')
        scores_pd = score_utils.compare_kl_divergences(dists_computed)
        if not os.path.exists(dir_save):
            os.mkdir(dir_save)
        scores_filepath = os.path.join(dir_save, 'results.csv')
        scores_pd.to_csv(scores_filepath)

    print(scores_pd)

    return scores_pd


def create_and_run_bn(dataframe, train_structure, train_structure_mode, graph_filepath, train_parameters,
                      export_parameters, inference, inference_method, test_multiple_nets, evidences, marginals):

    if test_multiple_nets:
        run_multiple_nets(dataframe)
        exit(0)

    bn = BN(dataframe)
    cols_names = bn.dataframe.columns.values

    if train_structure:
        bn.learn_structure("fges", mode=train_structure_mode)
    else:
        bn.read_structure_from_file(graph_filepath)

    if train_parameters:
        bn.learn_parameters("mle")
        if export_parameters:
            bn.export_parameters()
    else:
        raise Exception("Load saved parameters is not implemented yet")

    if inference:
        mu_marginal, sigma_marginal = bn.run_inference(inference_method, evidences, marginals)
    else:
        print("Inferece set to NOT run")

    return 0

if __name__ == '__main__':
    print("Reading input data...")
    #data_csv_filepath = "data/net1_expression_data.tsv"  # "data/gaussian_test.csv"
    #dataframe = pd.read_csv(data_csv_filepath, na_filter=False, dtype=np.float64, low_memory=False, sep='\t')
    #dataframe = pd.read_csv("data/gaussian_test.csv", na_filter=False, dtype=np.float64, low_memory=False)
    dataframe = pd.read_parquet("data/clean_full_brain.parquet.gzip", engine="fastparquet").astype(np.float64)
    dataframe = dataframe.iloc[:, 1:]
    cols_names = dataframe.columns.values
    #run_multiple_nets(dataframe)

    train_structure = False
    graph_filepath = 'local_graphs/0_modified.gzip'
    train_structure_method = "fges"
    train_structure_mode = "local-global"

    train_parameters = True
    export_parameters = True

    inference = False
    inference_method = "exact"
    evidences = {} #{cols_names[3]: 1.0}
    marginals = cols_names #[0:2]
    test_multiple_nets = False

    create_and_run_bn(dataframe, train_structure, train_structure_mode, graph_filepath, train_parameters,
                      export_parameters, inference, inference_method, test_multiple_nets, evidences, marginals)

    done = True

