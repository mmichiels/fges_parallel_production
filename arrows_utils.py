import numba

SEP_NUMS = '_'
SEP_I_J = ':'
SEP_SETS = ';'

def create_arrow_str(arrow_input):
    x = arrow_input[0][0]
    y = arrow_input[0][1]
    NaYX = list(arrow_input[1])
    T = list(arrow_input[2])
    bic = arrow_input[3]

    arrow = (
        str(x) + SEP_NUMS + str(y) + SEP_I_J +  # i, j
        SEP_NUMS.join((str(node) for node in NaYX)) + SEP_SETS + # set(NaYX)
        SEP_NUMS.join((str(node) for node in T)),  # set(T)
        bic                                        # BIC score buff
    )

    return arrow

def get_all_vals(arrow):
    x = get_i(arrow)
    y = get_j(arrow)
    NaYX = get_NaYX(arrow)
    T = get_T(arrow)
    bic = get_bic(arrow)

    return x, y, NaYX, T, bic

def get_arrow_str(arrow):
    return arrow[0]

def get_bic(arrow):
    return arrow[1]

def get_i_j_str(input):
    i_j_str = input.split(SEP_I_J)[0]

    return i_j_str

def get_sets_str(input):
    sets_str = input.split(SEP_I_J)[1]

    return sets_str

def get_i_j(arrow):
    i = get_i(arrow)
    j = get_j(arrow)

    return i, j

def get_i(arrow):
    input = get_arrow_str(arrow)
    i_j_str = get_i_j_str(input)
    i = i_j_str.split(SEP_NUMS)[0]

    return int(i)

def get_j(arrow):
    input = get_arrow_str(arrow)
    i_j_str = get_i_j_str(input)
    j = i_j_str.split(SEP_NUMS)[1]

    return int(j)

def get_NaYX(arrow):
    input = get_arrow_str(arrow)
    sets_str = get_sets_str(input)
    NaYX_str = sets_str.split(SEP_SETS)[0]
    if len(NaYX_str) == 0:
        NaYX = set({})
    else:
        NaYX = set([int(s) for s in NaYX_str.split(SEP_NUMS)])

    return NaYX

def get_T(arrow):
    input = get_arrow_str(arrow)
    sets_str = get_sets_str(input)
    T_str = sets_str.split(SEP_SETS)[1]
    if len(T_str) == 0:
        T = set({})
    else:
        T = set([int(s) for s in T_str.split(SEP_NUMS)])

    return T

