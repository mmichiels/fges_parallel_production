import json

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mutual_info_score
import scipy.stats as scipy_stats
#from line_profiler import LineProfiler
from profilehooks import timecall
from profilehooks import profile
# from memory_profiler import profile as mem_profile
from pympler import tracker
from pympler import asizeof as aof
import numba
#from numba.pycc import CC
from decorator import decorator
from itertools import repeat
import multiprocessing as mp
#import torch
import os
import mkl
import time
import networkx as nx
import networkx.readwrite as networkx_io
import numpy as np
from itertools import combinations, permutations
import time
import pandas as pd
import random

def generate_random_color():
    random_color = lambda: random.randint(0, 255)
    random_color_hex = ('#%02X%02X%02X' % (random_color(), random_color(), random_color()))
    return random_color_hex

def create_features_categories(df, filename, alternative_names, cols_features, col_score, col_id):
    discrete_features = {}
    for feature_name in cols_features:
        feature = df.loc[:, feature_name].values
        categories = np.unique(feature)
        discrete_features[feature_name] = {}

        for category in categories:
            if category:
                discrete_features[feature_name][category] = {
                    "color": generate_random_color(),
                }

    nodes_names = np.unique(df.loc[:, col_id].values)
    nodes_associations = {}
    for i, node_symbol in enumerate(nodes_names):
        print("Processed feature {} of {}".format(i+1, len(nodes_names)))
        if node_symbol not in alternative_names:
            continue
        node_entrez = alternative_names[node_symbol]
        nodes_associations[node_entrez] = {"alternative_name": node_symbol}
        if col_score is not None:
            features_node = df.loc[df[col_id] == node_symbol].loc[:, cols_features + [col_score]]
            features_node.sort_values(by=[col_score], ascending=False, inplace=True)
        else:
            features_node = df.loc[df[col_id] == node_symbol].loc[:, cols_features]
        features_node.drop_duplicates(subset=cols_features, inplace=True)
        if any(features_node):
            nodes_associations[node_entrez]["discrete_features"] = {}
            for feat_name in cols_features:
                feat = features_node.loc[:, feat_name]
                if any(feat):
                    nodes_associations[node_entrez]["discrete_features"][feat_name] = []
                    categories_feat_node = feat.values
                    for category in categories_feat_node:
                        if category:
                            nodes_associations[node_entrez]["discrete_features"][feat_name].append(category)

    additional_parameters = {
        "discrete_features": discrete_features,
        "nodes": nodes_associations,
    }

    with open("./data/{}.json".format(filename), 'w') as output_file_json:
        json.dump(additional_parameters, output_file_json)

    return 0

if __name__ == '__main__':
    """
    Data from:
    http://www.disgenet.org/browser/0/0/1/0/source__CURATED/_b./
    http://www.disgenet.org/browser/0/0/3/0/source__CURATED/_b./
    """
    METADATA_FILENAME = "metadata_genes_proteins" #metadata_genes_diseases
    ALTERNATIVE_NAMES_FILENAME = "Gene_Names_output"

    if METADATA_FILENAME == "metadata_genes_proteins":
        metadata_df = pd.read_csv("data/browser_source_genes_summary_CURATED.tsv", na_filter=False, low_memory=False, sep='\t')
        col_id = "symbol"
        cols_features = ["protein_class"]
        cols = [col_id] + cols_features
        col_score = None
    elif METADATA_FILENAME == "metadata_genes_diseases":
        metadata_df = pd.read_csv("data/browser_source_summary_gda_CURATED.tsv", na_filter=False, low_memory=False, sep='\t')
        col_id = "symbol"
        cols_features = ["disease_name"]
        col_score = "score"
        cols = [col_id] + cols_features + [col_score]
    else:
        raise Exception("Metadata {} not found. Choose metadata_genes_proteins or metadata_genes_diseases".format(METADATA_FILENAME))

    with open('./data/{}.json'.format(ALTERNATIVE_NAMES_FILENAME)) as alternative_names_file:
        alternative_names = json.load(alternative_names_file)

    metadata_df = metadata_df.loc[:, cols]

    create_features_categories(metadata_df, METADATA_FILENAME, alternative_names, cols_features, col_score, col_id)

