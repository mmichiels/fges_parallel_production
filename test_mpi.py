from mpi4py import MPI

#-----MPI setup------------
COMM = MPI.COMM_WORLD
size = COMM.Get_size()
rank = COMM.Get_rank()
node_name = MPI.Get_processor_name()
mpi_info = MPI.Info.Create()
mpi_version = MPI.get_vendor()

print("########### MPI version: {} -- Node: {}; rank {} of {} #############".format(mpi_version, node_name, rank, size-1))
