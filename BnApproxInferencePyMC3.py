
from graph_utils import *

#============Theano config}=====================
import os

#os.environ["DEVICE"] = 'cuda0'
os.environ["OMP_NUM_THREADS"] = '8'
os.environ["THEANO_FLAGS"] = "mode=FAST_COMPILE,optimizer_excluding=inplace,optimizer_excluding=inplace_elemwise_optimizer" \
                             ",optimizer_including=local_remove_all_assert," \
                             "allow_gc=False,dnn.enabled=False,cycle_detection=fast,openmp=True,compute_test_value=off"#,floatX=float32,device=cuda0"
#os.environ["MKL_THREADING_LAYER"] = "GNU"
import theano
theano.config.compute_test_value = 'off'

#theano.config.mode = 'FAST_RUN'
#================================================
import pymc3 as pm
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import networkx as nx


class ModelPyMC3:
    def __init__(self, bn_model, nodes_by_order=None):
        self.params = bn_model.parameters
        self.model = pm.Model()
        self.cols_names = bn_model.dataframe.columns.values.tolist()
        self.graph = bn_model.graph

        num_nodes_created = 0
        if nodes_by_order is None:
            nodes_order = get_nodes_by_num_parents(self.graph)
            nodes_by_order = []
            for node_idx, num_parents in nodes_order:
                node_name = self.cols_names[node_idx]
                num_nodes_created = self.create_node_recursive(node_name, num_nodes_created, nodes_by_order)

            nodes_by_order_np = np.array(nodes_by_order)
            np.save('./nodes_by_order.npy', nodes_by_order_np)
        else:
            #for node_name in nodes_by_order:
            #    num_nodes_created = self.create_node_recursive(node_name, num_nodes_created, nodes_by_order)

            n = 100
            for i in range(n):
                with self.model:
                    node = pm.Normal('node_{}'.format(i), 1, 1)
                print("Creating node {} of {}".format(i, n))

        print("Model PyMC3 created!")

    def create_node_recursive(self, node_name, num_nodes_created, nodes_by_order):
        node_mean = self.params[node_name].mean
        node_var = self.params[node_name].var
        parents_names = self.params[node_name].parents_names
        parents_coeffs = self.params[node_name].parents_coeffs

        if len(parents_names) > 0:
            node_mean = parents_coeffs[0]  # Intercept
            for i, parent in enumerate(parents_names):
                if parent not in self.model.named_vars:
                    print("Recursive in: {} of {}".format(num_nodes_created, len(self.cols_names)))
                    num_nodes_created = self.create_node_recursive(parent, num_nodes_created, nodes_by_order)
                node_mean += parents_coeffs[i] * self.model[parent]  # Use parent already created in this model

        if node_name not in self.model.named_vars:
            with self.model:
                node = pm.Normal(node_name, node_mean, node_var)
            nodes_by_order.append(node_name)
            print("CREATED node: {} of {}".format(num_nodes_created, len(self.cols_names)))
            num_nodes_created += 1

        return num_nodes_created

    def plot_graph(self):
        graph_pymc3 = pm.model_to_graphviz(self.model)
        graph_pymc3.render(format="svg", view=True, cleanup=True)

        return 0

    def run_inference(self, algorithm_name="NUTS", show_plots=True):
        print("=====Inference with: {}=======".format(algorithm_name))
        print("Starting {}...".format(algorithm_name))

        if algorithm_name=="NUTS":
            step = pm.NUTS(model=self.model, profile=True)
        elif algorithm_name == "metropolis":
            step = pm.Metropolis(model=self.model)
        elif algorithm_name == "variational":
            vi_est = pm.fit(n=10, model=self.model)  # Run ADVI
            vi_trace = vi_est.sample()  # sample from VI posterior

            if show_plots:
                plt.plot(vi_est.hist)
                plt.ylabel('ELBO')
                plt.xlabel('iteration')
                #pm.traceplot(vi_trace)
                plt.show()
            return 0
        else:
            raise Exception("Algorithm {} is not supported".format(algorithm_name))

        print("Sampling...Please wait...")
        trace = pm.sample(draws=1000, step=step, model=self.model)

        if show_plots:
            pm.plots.traceplot(trace=trace)
            plt.show()

        return 0

if __name__ == '__main__':
    print("Inference starts!")

    # Model definition:
    parent_0_mean = 1.0
    parent_1_mean = 2.0
    with pm.Model() as model:
        parent_0 = pm.Normal('parent_0', mu=parent_0_mean, sd=1.0)
        parent_1 = pm.Normal('parent_1', mu=parent_1_mean, sd=1.0)

        coeffs = [0, 1, 1] #i=0 is the intercept
        child_mean = coeffs[0] + coeffs[1]*parent_0 + coeffs[2]*parent_1
        print("Child mean: ", child_mean)
        child = pm.Normal('child', mu=child_mean, sd=1.0)

        graph_pymc3 = pm.model_to_graphviz(model)
        graph_pymc3.render(format="png", view=True, cleanup=True)

    with model:
        parent_0.observed = False
        #start = pm.find_MAP() #MAP should't be used to start parameters with NUTS sampling
        trace = pm.sample(draws=1000, step=pm.NUTS())

        #child_1_samples = trace['child']
        print("Child mean: ", coeffs[0] + coeffs[1]*parent_0_mean + coeffs[2]*parent_1_mean)
        #print("Posterior mean child_1: ", child_1_samples.mean())
        pm.plots.traceplot(trace=trace)
        plt.show()


    done = True