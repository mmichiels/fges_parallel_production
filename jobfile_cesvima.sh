#!/bin/bash

#SBATCH --job-name=FGES_parallel_job
#SBATCH --ntasks=8 --nodes=8 ##MPI processes: --ntasks is equivalent to the "mpirun -np" option. --ntasks and --nodes must be the same in our case because we want 1 process per node
#SBATCH --cpus-per-task=8 ##OpenMP processes: number of threads (each one in a core) per process
#SBATCH --output=./log_jobfile.log
## SBATCH --time=00:30:00 ##DD-HH:MM:SS
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cig.upm@gmail.com

module purge
module load GCC/6.4.0-2.28
module load OpenMPI/2.1.2-GCC-6.4.0-2.28

srun python FGES_parallel.py

## To run this, in cmd run:
## sbatch ./jobfile_cesvima.sh

## To monitor the status of the task:
## squeue -u $USER
## Or in real time:
## multitail -rc 3 -ts -l "squeue -u $USER"
