#!/usr/bin/env bash

MPICH_BIN_PATH='/usr/bin/mpirun'
CONDA_BIN_PATH='/opt/conda/bin/conda'
CONDA_ENV='conda_py364_fges'

function install_mpi_and_dependencies() {
    #MPICH setup:
    chmod +x setup_cluster_mpi_master.sh
    chmod +x setup_cluster_mpi_slave.sh

    cd /

    echo "Installing graphviz dependencies..."
    apt-get install -y python-pygraphviz graphviz libgraphviz-dev libxml2-dev
    echo "Graphviz dependencies installed!"

    if [ -f $MPICH_BIN_PATH ];
    then
        echo "MPI is already installed in your system. Please be sure that your installation is MPICH 3.2 by running 'mpirun --version'. If not, please
        uninstall your previous MPI installation (by doing something like apt-get purge mpich or apt-get purge openmpi-bin
        and then run this script again."
    else
        echo "MPI installation not found in your system"
        echo "Installing MPICH_3.2..."
        wget http://archive.ubuntu.com/ubuntu/pool/universe/m/mpich/mpich_3.2.orig.tar.gz
        tar -xvf mpich_3.2.orig.tar.gz
        rm mpich_3.2.orig.tar.gz
        cd mpich-3.2
        ./configure --prefix=/usr/
        make -j all #-j all is to use all available cores
        make install
        echo "MPICH_3.2 installation finished!"
    fi

    echo ""
}

function install_conda() {
    if [ -f $CONDA_BIN_PATH ];
    then
        echo "Conda is already installed in your system"
    else
        echo "Conda not found in the system."
        echo "Installing conda..."

        cd / && \
        wget https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
        /bin/bash ~/miniconda.sh -b -p /opt/conda && \
        rm ~/miniconda.sh && \
        /opt/conda/bin/conda clean -tipsy && \
        ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
        echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
        export PATH=$CONDA_BIN_PATH:$PATH

        echo "Conda installation finished!"
    fi

    echo ""
}

function update_conda_env() {
    echo "Updating conda environment..."

    $CONDA_BIN_PATH env update -f ./conda_environment.yml
    if [ $? -eq 0 ]; then
        echo "Conda environment updated!"
        echo ""
    else
        echo "Conda environment is not created, please run the following and the run this script again: "
        echo "source ~/.bashrc"
        echo "conda env create python=3.6.4 -f ./conda_environment.yml"
       exit 1
    fi

    #Can't run the following because conda command is not found:
    #$CONDA_BIN_PATH env create python=3.6.4 -f ./conda_environment.yml
    #$CONDA_BIN_PATH activate $CONDA_ENV
}

function running_instructions() {
    echo "Installation DONE!"

    #Conda activate <env_name>:
    echo "Please now activate the conda environment by doing: "
    echo "echo 'conda activate $CONDA_ENV' >> ~/.bashrc"
    echo "source ~/.bashrc"

    echo "conda activate $CONDA_ENV"
    echo ""

    #As single node:
    echo "To run the program in a single node just type: python FGES-MB_parallel.py"
    echo ""

    #As multiple nodes with MPI in a custom cluster:
    echo "To run the program in multiple nodes (in a CUSTOM cluster computing environment) through MPI:"
    echo "- Run the setup_cluster_mpi_master.sh in the master node"
    echo "- Run the setup_cluster_mpi_slave.sh in every slave node"
    echo "- Fill the hosts_mpi.conf with the proper names of your MPI computing nodes. The first line corresponds
    to the master node, the others to the slaves"

    echo "Then to run the program type:"
    echo "mpirun -np <num_nodes> --hostfile hosts_mpi.conf python FGES-MB_parallel.py"
    echo ""

    echo "IMPORTANT: if the mpirun command hangs, please verify that there is not a firewall blocking the MPI communications. For example check that ufw is not blocking the
    connections, to test this, run 'ufw disable' to disable the firewall and then verify that the mpirun command now works."

    #As multiple nodes with MPI in a HCP environment:
    echo "To run the program in multiple nodes (in a HPC cluster computing environment) through MPI:"
    echo "- Run the ./jobfile_cesvima.sh"
    echo "- If there are any errors, please check the configuration content of the ./jobfile_bsc.sh file"
}
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "ERROR: This script must be run as root" 1>&2
   exit 1
fi

install_mpi_and_dependencies
install_conda
update_conda_env
running_instructions