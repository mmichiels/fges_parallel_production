import json

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mutual_info_score
import scipy.stats as scipy_stats
#from line_profiler import LineProfiler
from profilehooks import timecall
from profilehooks import profile
# from memory_profiler import profile as mem_profile
from pympler import tracker
from pympler import asizeof as aof
import numba
#from numba.pycc import CC
from decorator import decorator
from itertools import repeat
import multiprocessing as mp
#import torch
import os
import mkl
import time
import networkx as nx
import networkx.readwrite as networkx_io
import numpy as np
from itertools import combinations, permutations
import time
import pandas as pd
import random

def entrez_to_symbol(df, filename_output, col_entrez_id, col_symbol):
    df.dropna(inplace=True)
    df.drop_duplicates(subset=col_entrez_id, inplace=True)
    df.reset_index(drop=True, inplace=True)
    df = df.astype({col_entrez_id: np.int64}, inplace=True)
    df.to_csv('data/{}.csv'.format(filename_output))

    df_dict = {}
    for entrez_id, symbol in df.values:
        df_dict[symbol] = entrez_id
    with open('data/{}.json'.format(filename_output), 'w') as output_file_json:
        json.dump(df_dict, output_file_json)

    return 0

if __name__ == '__main__':
    FILENAME_INPUT = "Gene_Names"
    FILENAME_OUTPUT= "Gene_Names_output"
    df = pd.read_csv("data/Gene_Names.csv", low_memory=False)

    col_entrez_id = "entrez_id"
    col_symbol = "gene_symbol"
    cols = [col_entrez_id, col_symbol]
    df = df.loc[:, cols]

    entrez_to_symbol(df, FILENAME_OUTPUT, col_entrez_id, col_symbol)

