#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>
#include <mpi.h>

int main(int argc, char **argv){
    int rank, size;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    printf("Soy el proceso MPI %d de %d. Estoy en el core %d del nodo %s.\n",
    rank, size, sched_getcpu(), getenv("SLURM_NODEID"));
    sleep(5);
    MPI_Finalize();
    return 0;
}
