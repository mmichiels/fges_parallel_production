# Check the full union graph, and think about how to simplify it to do backward search

import pandas as pd
import FGES_parallel
import numpy as np


BICS = pd.read_csv("/home/cig/Desktop/FGES_parallel/fges_parallel/data/test_bics.csv", na_filter=False, dtype=np.float64, low_memory=False).values
print("BICS loaded")
data = pd.read_csv("/home/cig/Desktop/FGES_parallel/fges_parallel/data/clean_full_brain.csv", na_filter=False, dtype=np.float64, low_memory=False).iloc[:, 1:].values
print("Data loaded")

n = BICS.shape[0]

graph = [1990, 1991, 1992]
all_BICS = np.zeros((n, n))
for i in range(n):
    for j in graph:
        if i == j:
            continue
        all_BICS[j, i] = FGES_parallel.BIC_numba(data[:, i], data[:, j], 40.0)
        all_BICS[i, j] = all_BICS[j, i]
for i in graph:
    print(np.allclose(all_BICS[:, i], BICS[:, i]))