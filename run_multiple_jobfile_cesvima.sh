#!/bin/bash

DATE_NOW=`date +%d_%m_%Y_%H_%M_%S`

NUM_TASKS=10
for ((i=1;i<=NUM_TASKS;i++)); do
    sbatch ./jobfile_cesvima.sh
    echo "Batch MPI task $i sent to the queue; $DATE_NOW" >> log_mpi_tasks_cesvima.log
done
