from graph_utils import *
import numpy as np
import matplotlib
from scipy.stats import multivariate_normal as mvn
from scipy.integrate import nquad
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import networkx as nx

def topological_sort(graph):
    # Returns a topological ordering of the graph
    def sortUtil(graph, node, visited, stack):
        visited[node] = True
        for child in children(graph, node):
            if not visited[child]:
                sortUtil(graph, child, visited, stack)
        stack.insert(0, node)

    n = graph.shape[0]
    visited = [False]*n
    stack = []
    for node in range(n):
        if not visited[node]:
            sortUtil(graph, node, visited, stack)

    return stack


def joint(BN):
    # Takes the factorized distribution implied by the BN and creates the join gaussian.

    Mu = np.zeros((BN.graph.shape[0],))
    Sigma = np.zeros(BN.graph.shape)

    order = topological_sort(BN.graph)
    for node in order:
        node_name = BN.dataframe.columns[node]
        parameters = BN.parameters[node_name]
        mean, var, parents_coeffs, parents = parameters.mean, parameters.var, parameters.parents_coeffs, parameters.parents_names
        parents = [np_list_index(BN.dataframe.columns.values, i) for i in parents]
        Mu[node] = mean + sum([Mu[i]*j for i, j in zip(parents, parents_coeffs)])
        if parents:
            cov_parents_involved = Sigma[:, parents]
            cov_parents = cov_parents_involved[parents, :]
            Sigma[:, node] = np.dot(cov_parents_involved, np.array(parents_coeffs))
            Sigma[node, node] = var + np.dot(np.array(parents_coeffs), np.dot(cov_parents, np.array(parents_coeffs)))
            Sigma[node, :] = Sigma[:, node]
        else:
            Sigma[node, node] = var

    return Mu, Sigma, order


def condition_on_evidence(Mu, Sigma, node_names, evidences):
    # Conditions a multivariate gaussian on some evidences
    evidences_nodes = list(evidences.keys())
    evidences_vals = [evidences[node] for node in evidences_nodes]
    order_nodes = [x for x in node_names if x not in evidences.keys()]

    """Divide the covariance matrix into blocks xx, xy, yy for xx: nodes with evidences, yy:nodes wo evidences"""
    indices = list(range(Sigma.shape[0]))
    idx_sigma = np.array([node_names[i] in evidences_nodes for i in indices])

    sigma_xy = Sigma[idx_sigma, :][:, ~idx_sigma]
    sigma_xx = Sigma[idx_sigma, :][:, idx_sigma]
    sigma_inv = np.linalg.solve(sigma_xx, sigma_xy)
    sigma_yy = Sigma[~idx_sigma, :][:, ~idx_sigma]

    # Compute conditional distribution
    mu_y = Mu[~idx_sigma] + np.dot(sigma_inv.T, (evidences_vals - Mu[idx_sigma]))
    sigma_y = sigma_yy - np.dot(sigma_xy.T, sigma_inv)

    return mu_y, sigma_y, order_nodes


def np_list_index(array, value):
    return np.where(array == value)[0][0]


def marginal(mu_joint, sigma_joint, all_nodes_names, nodes_names):
    nodes_names = np.array(nodes_names)
    idx_mu = [np_list_index(nodes_names, node_name) for node_name in nodes_names]
    indices = list(range(sigma_joint.shape[0]))
    idx_sigma = np.array([all_nodes_names[i] in nodes_names for i in indices])

    mu_marginal = mu_joint[idx_mu]
    sigma_marginal = sigma_joint[idx_sigma, :][:, idx_sigma]

    return mu_marginal, sigma_marginal


def condition_on_ineq(Mu, Sigma, node_names, evidences, mode="greater"):
    # Takes greater than or lower than as modes. Conditions a joint distribution on inequality evidence and returns
    # the posterior. P(Y|X>x) = P(X>x|Y)P(Y)/P(X>x)
    if mode not in ["greater", "lower"]:
        raise ValueError("Conditioning mode can only be greater or lower")

    evidences_nodes = list(evidences.keys())
    evidences_vals = [evidences[node] for node in evidences_nodes]
    order_nodes = [x for x in node_names if x not in evidences.keys()]

    # Get the marginals for X and Y
    mu_x, sigma_x = marginal(Mu, Sigma, node_names, evidences_nodes)
    p_X = mvn(mean=mu_x, cov=sigma_x)
    cdf_X = p_X.cdf(np.array(evidences_vals))
    if mode is "greater":
        cdf_X = 1 - cdf_X

    mu_y, sigma_y = marginal(Mu, Sigma, node_names, order_nodes)
    p_Y = mvn(mean=mu_y, cov=sigma_y)

    def posterior(y, post_mode="equal"):
        if post_mode not in ["map", "equal", "greater", "lower"]:
            raise ValueError("Mode can only be map, equal (pdf), lower or greater (cdf)")

        if post_mode is "map":
            # I think this is just mu_y, but need to check
            return mu_y

        elif post_mode is "equal":
            # Get inverse conditional P(X|Y)
            evidence_inverse = {name:y[i] for i, name in enumerate(order_nodes)}
            mu_xy, sigma_xy, _ = condition_on_evidence(Mu, Sigma, node_names, evidence_inverse)
            cdf_X_if_Y = mvn(mean=mu_xy, cov=sigma_xy).cdf(np.array(evidences_vals))
            if mode is "greater":
                cdf_X_if_Y = 1 - cdf_X_if_Y

            # Return pdf for P(Y=y|X>x)
            return p_Y.pdf(np.array(y))*cdf_X_if_Y/cdf_X

        else:
            def integrand(*args):
                value_y = [arg for arg in args]
                return posterior(value_y)

            # Use pdf mode and integrate to obtain cdf
            if post_mode is "lower":
                ranges = [(mu_y[i] - 10*sigma_y[i, i], y[i]) for i in range(len(y))]
                cdf = nquad(integrand, ranges)

            else:
                ranges = [(y[i], mu_y[i] + 10*sigma_y[i, i]) for i in range(len(y))]
                cdf = nquad(integrand, ranges)

            return cdf

    # Returns a function from where you can query the pdf, cdf and map of the posterior.
    return posterior





