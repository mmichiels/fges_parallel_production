
from graph_utils import *
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import networkx as nx
import torch
import pyro
import pyro.distributions as dist
import pyro.poutine as poutine
from pyro.distributions.util import logsumexp
from pyro.infer import EmpiricalMarginal, SVI, Trace_ELBO
from pyro.infer.abstract_infer import TracePredictive
from pyro.contrib.autoguide import AutoMultivariateNormal, AutoDiagonalNormal
from pyro.infer.mcmc import HMC, MCMC, NUTS
import pyro.optim as optim
import pyro.poutine as poutine
from pyro.contrib.autoguide import AutoDiagonalNormal

pyro.set_rng_seed(101)

class ModelPyro:
    def __init__(self, bn_model, cuda=False):
        self.params = bn_model.parameters
        self.cols_names = bn_model.dataframe.columns.values.tolist()
        self.graph = bn_model.graph
        if cuda:
            print("Running in CUDA")
            torch.set_default_tensor_type('torch.cuda.DoubleTensor')
        else:
            print("Running in CPU")

        num_nodes_created = 0
        #if nodes_by_order is None:
        self.nodes_by_num_parents = get_nodes_by_num_parents(self.graph)
        self.nodes_by_order = []
        self.create_model_unknown_order()
        self.model = self.create_model_unknown_order

            #nodes_by_order_np = np.array(self.nodes_by_order)
            #np.save('./nodes_by_order.npy', nodes_by_order_np)
        """
        else:
            self.nodes_by_order = nodes_by_order
            self.model = self.create_model_known_order
        """
        """
        trace = poutine.trace(self.model).get_trace()
        for node, val in trace.nodes.items():
            print(node, val)
        for edge in trace.edges:
            print(edge)
        """

        print("ModelPyro created!")

    def create_model_unknown_order(self):
        num_nodes_created = 0
        self.pyro_nodes = {}
        for node_idx, num_parents in self.nodes_by_num_parents:
            node_name = self.cols_names[node_idx]
            num_nodes_created = self.create_node_recursive(node_name, num_nodes_created)

    def create_model_known_order(self):
        num_nodes_created = 0
        self.pyro_nodes = {}
        #for i in pyro.plate("locals", len(self.cols_names)):
        #    node_name = self.cols_names[i]
        for node_name in self.nodes_by_order:
            num_nodes_created = self.create_node_recursive(node_name, num_nodes_created)
        #print("One loop done!")

    def create_node_recursive(self, node_name, num_nodes_created):
        node_mean = self.params[node_name].mean
        node_var = self.params[node_name].var
        parents_names = self.params[node_name].parents_names
        parents_coeffs = self.params[node_name].parents_coeffs

        if len(parents_names) > 0:
            node_mean = parents_coeffs[0]  # Intercept
            for i, parent in enumerate(parents_names):
                if parent not in self.pyro_nodes:
                    #print("Recursive in: {} of {}".format(num_nodes_created, len(self.cols_names)))
                    num_nodes_created = self.create_node_recursive(parent, num_nodes_created)
                node_mean += torch.tensor(parents_coeffs[i]) * self.pyro_nodes[parent]  # Use parent already created in this model

        if node_name not in self.pyro_nodes:
            self.pyro_nodes[node_name] = pyro.sample(node_name, dist.Normal(node_mean, node_var))
            self.nodes_by_order.append(node_name)
            #print("CREATED node: {} of {}: {}".format(num_nodes_created, len(self.cols_names),self.pyro_nodes[node_name]))
            num_nodes_created += 1

        return num_nodes_created

    def test_cond_model(self):
        parent_0_mean = 1
        parent_1_mean = 2
        parent_0 = pyro.sample('parent_0', dist.Normal(parent_0_mean, 1.0))
        parent_1 = pyro.sample('parent_1', dist.Normal(parent_1_mean, 1.0))

        coeffs = [0, 1, 1] #i=0 is the intercept
        child_mean = coeffs[0] + coeffs[1]*parent_0 + coeffs[2]*parent_1
        child = pyro.sample('child', dist.Normal(child_mean, 1.0))

    def test_indep_model(self):
        n = 10
        with pyro.plate('x_independent', n):
            pyro.sample("x", dist.Normal(0, 1))

    def run_inference(self, algorithm_name="NUTS", show_plots=True, evidences=None, marginals=None):
        print("=====Inference with: {}=======".format(algorithm_name))
        print("Starting {}...".format(algorithm_name))
        if marginals is None:
            marginals = self.cols_names
        mu_marginal = []
        sigma_marginal = []

        if algorithm_name == "NUTS":
            nuts_kernel = NUTS(self.model, adapt_step_size=True, jit_compile=False)
            mcmc_run = MCMC(nuts_kernel, num_samples=20, warmup_steps=10).run()

            for node in marginals:
                posterior = mcmc_run.marginal(node).empirical[node]
                mu_marginal.append(float(posterior.mean))
                sigma_marginal.append(float(posterior.variance))
            #posterior = mcmc_run.marginal('x_independent')
            #print("NUTS posterior: ", posterior)

        elif algorithm_name == "variational":
            guide = AutoDiagonalNormal(self.model)
            optimizer = optim.Adam({"lr": 0.001})
            svi = SVI(self.model, guide, optimizer, loss=Trace_ELBO(), num_samples=100)

            pyro.clear_param_store()
            num_iters = 1000
            for i in range(num_iters):
                elbo_loss = svi.step()
                print("Elbo iter {} of {}; loss: {}".format(i, num_iters, elbo_loss))

            for key, posterior in pyro.get_param_store().items():
                if key == "auto_loc":
                    mu_marginal.append(posterior.tolist())
                else:
                    sigma_marginal.append(posterior.tolist())

        else:
            raise Exception("Algorithm {} is not supported".format(algorithm_name))

        print("Finished!")

        return mu_marginal, sigma_marginal
