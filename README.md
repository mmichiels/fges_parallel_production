We present two options to run the algorithm, the algorithm is exactly the same in both
options, the only difference is the computing environment where it's executed.

### **Online option:**
You can run the algorithm with your preferred dataset in our web server.
This is the best option if you just want to run the algorithm, as it
doesn't need any installation. It's the recommended option for small/medium
datasets.

- Upload your csv/Apache Parquet dataset file here:
https://neurosuites.com/morpho/select_upload_neurons

- Then in the left sidebar click in Machine learning -> Bayesian networks.
Select your features in the left side, click continue.

- Select the FGES algorithm to learn the structure with the algorithm presented here.

- Select the Gaussian MLE to learn the parameters.

- You will have an interactive bayesian network in the bottom of the page.

### **Local option:**
This is the best option if you want to modify the source code to fit your
your additional requirements. It's the recommended option for medium/large
datasets because you can run the code in a computing cluster (if you have one).

#### **Automatic Installation:**

##### To install this program automatically in your personal computer, just run as root: ########
```
chmod +x ./install.sh 
./install.sh
```

##### To install this program automatically in a HPC environment, just run as root: ########
```
chmod +x ./install_bsc.sh 
./install_bsc.sh
```

#### **Running instructions:**

##### Run in a single computing node (single computer)

```
python FGES-MB_parallel.py
```

##### Run in multiple computing nodes in a cluster computing environment through MPI
- Run the setup_cluster_mpi_master.sh in the master node
- Run the setup_cluster_mpi_slave.sh in every slave node
- Fill the hosts_mpi.conf with the proper ips of your MPI computing nodes. The first line corresponds
to the master node, the others to the slaves

- IMPORTANT: if the mpirun command hangs, please verify that there is not a firewall blocking the MPI communications. 
For example check that ufw is not blocking the connections, to test this, run 'ufw disable' to disable the firewall and then verify that the mpirun command now works.

```
mpirun -np <num_nodes> --hostfile hosts_mpi.conf python FGES-MB_parallel.py
```

##### Debug

In Pycharm, create  a new debug configuration. The python interpreter
path is:

```
/opt/conda/envs/conda_py364_fges/bin/python
```

To run the program in a single node just type: 
 ```
python FGES-MB_parallel.py"
 ```

#### **Manual Installation:**

Just in case the automatic installation fails, read the following instructions:

##### Install MPI for parallelism between nodes in a computer cluster
```
wget http://archive.ubuntu.com/ubuntu/pool/universe/m/mpich/mpich_3.2.orig.tar.gz
tar -xvf mpich_3.2.orig.tar.gz
rm mpich_3.2.orig.tar.gz
cd mpich-3.2
./configure --prefix=/usr/
make -j all #-j all is to use all available cores
make install
echo "MPICH_3.2 installation finished!"
```

##### Install Conda (skip this step if you already have conda installed in your system)

```
cd / && \
wget https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
/bin/bash ~/miniconda.sh -b -p /opt/conda && \
rm ~/miniconda.sh && \
/opt/conda/bin/conda clean -tipsy && \
ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
export PATH=/opt/conda/bin:$PATH
```


##### Install packages and deploy the new conda virtual environment

```
apt-get install -y python-pygraphviz graphviz libgraphviz-dev libxml2-dev

cd fges_parallel
conda env create python=3.6.4 -f ./conda_environment.yml
conda activate conda_py364_fges
```

##### Update existing conda environment
Only just in case you already installed this package. In order to
update it, run:

```
cd fges_parallel
git pull
conda env update -f ./conda_environment.yml
```
 
##### Why are arrows internally represented as strings?
It is just a memory optimization to allow the algorithm to run with large networks (> 20.000 nodes)
in a single computer with <= 64 GB RAM.

##### Development notes about the parallelism:

- We use Numba to parallelize numerical functions because it allow us to release the GIL and hence creating threads.
Numba does this by using their jit (just in time) compilation so at the end the running code is no Python code (it's
already compiled code by Numba). This way we can create threads avoiding the GIL because (under the hood) we aren't
running Python code anymore.
    - The number of threads is set automatically by Numba (it's the same number as the one in the Python multiprocessing library).

 - We use MPI to parallelize the code over multiple nodes in a cluster. We use mpi4py==3.0.0 with MPICH==3.2.
 
